export class Car {
  constructor (id, make, model, price, powerHp) {
    this.id = id
    this.make = make
    this.model = model
    this.price = price
    this.powerHp = powerHp
  }
}
