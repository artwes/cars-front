export class CarSimple {
  constructor (id, model, make) {
    this.id = id
    this.model = model
    this.make = make
  }
}
