import { MakesService } from './make.service.js'
import { CarsService } from './cars.service.js'

$(document).ready(function () {
  $('#cars-table').hide()
  var makeService = new MakesService()
  var carsService = new CarsService()

  makeService.getMakes().then((makes) => {
    var html = '<thead><tr><th>Make</th></tr></thead><tbody>'
    makes.forEach(function (make) {
      html += '<tr id="' + make.id + '" class="makes"><td>' + make.name + '</td></tr>'
    })
    html += '</tbody>'
    $('#makes-table').html(html)
  })

  $('.makes').on('click', 'tr', function () {
    var selectedMake = $(this).attr('id')
    if (selectedMake == null) {
      return
    }
    carsService.fetchCars(selectedMake).then(function (cars) {
      var html = '<thead><tr><th>Id</th><th>Model</th><th>Make</th></tr></thead>'
      cars.forEach(function (car) {
        html += '<tr id="' + car.id + '" class="cars"><td>' + car.id + '</td><td>' + car.model + '</td><td>' + car.make + '</td></tr>'
      })
      $('#cars-table').html(html)
    })
    $('#cars-table').show()
  })

  $('.cars').on('click', 'tr', function () {
    var selectedCar = $(this).attr('id')
    if (selectedCar == null) {
      return
    }
    carsService.fetchCar(selectedCar).then(function (car) {
      var html = '<tbody><tr><td>Id:</td>' + '<td>' + car.id + '</td></tr>' +
            '<tr><td>Make:</td>' + '<td>' + car.make + '</td></tr>' +
            '<tr><td>Model:</td>' + '<td>' + car.model + '</td></tr>' +
            '<tr><td>Price:</td>' + '<td>' + car.price + '</td></tr>' +
            '<tr><td>PowerHp:</td>' + '<td>' + car.powerHp + '</td></tr></tbody>'
      $('#car-info').html(html)
    })
  })
})
