import { Car } from './models/car.js'
import { CarSimple } from './models/carSimple.js'

export class CarsService {
  async fetchCars (make) {
    return window.fetch('http://localhost:8000/cars').then(response => response.json()).then(cars => {
      var carsList = []
      cars.forEach(function (car) {
        if (car.make === make) {
          carsList.push(new CarSimple(car.id, car.model, car.make))
        }
      })
      return carsList
    })
  }

  async fetchCar (id) {
    return window.fetch('http://localhost:8000/cars/' + id).then(response => response.json()).then(car => {
      return new Car(car.id, car.make, car.model, car.price, car.powerHp)
    })
  }
}
