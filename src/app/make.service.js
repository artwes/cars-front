import { Make } from './models/make.js'

export class MakesService {
  constructor () {
    this._makes = this.fetchMakes()
  }

  async fetchMakes () {
    return window.fetch('http://localhost:8000/make').then(response => response.json()).then(makes => {
      var makesList = []
      makes.forEach(function (make) {
        makesList.push(new Make(make.name))
      })
      return makesList
    })
  }

  getMakes () {
    return this._makes
  }
}
